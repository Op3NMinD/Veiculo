﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;


namespace Veiculo
{
    public class Veiculo
    {
            
        #region Atributos

        private string _matricula;
        private double _kilometragem;
        private double _capacidade;
        private double _contem;
        private int _viagens;
        private double _media;

        #endregion

        #region Propriedades

        public string Matricula
        {
            get { return _matricula; }
            set { _matricula = value; }
        }

        public double Kilometragem
        {
            get { return _kilometragem; }
            set { _kilometragem = value; }
        }


        public double Capacidade
        {
            get { return _capacidade; }
            set { _capacidade = value; }
        }

        public double Contem
        {
            get { return _contem; }
            set { _contem = value; }
        }

        public int Viagens
        {
            get { return _viagens; }
            set { _viagens = value; }
        }

        public double Media
        {
            get { return _media; }
            set { _media = value; }
        }

        #endregion

        #region Métodos

        public Veiculo() : this("??",0.0, 0.0) { }
        public Veiculo(Veiculo veiculo):this(veiculo.Matricula, veiculo.Capacidade, veiculo.Media) { }
        public Veiculo(string matricula, double capacidade, double media)
        {
            Matricula = matricula;
            Kilometragem = 0;
            Capacidade = capacidade;
            Contem = 0; 
            Viagens = 0;
            Media = media;
        }

  

        public void Encher(double valor)
        {
            if (Contem + valor >= Capacidade)
            {
                Contem = Capacidade;
            }
            else
            {
                Contem += valor;
            }
        }
    
        public double ConsumoPorLitro(double valor)
        {
            double aux = valor / 100;
            return aux;
        }

        public double KilometrosPercorridos(double media, double contem)
        {
            double aux = (contem * 100) / media;
            return aux;
        }
        public string ReservaCombustivel(double contem)
        {
            string aux;
            if (contem < 10)
                aux = "Sim";
            else
                aux = "Não";
            return aux;           
        }

        public int ValorEmPercentagem()
        {
            return (int)Math.Round(Contem / Capacidade * 100);
        }

        public double TotalGastoAteHoje(double precoLitro,double valoratual,double quant)
        {
            return valoratual + (quant * precoLitro);
        }

        public void Viajar(double km)
        {
            double aux = Media / 100 * km;
            Viagens++;
            Kilometragem += km;
            Contem -= aux;

        }

        #endregion
    }
}
