﻿namespace Veiculo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxMatricula = new System.Windows.Forms.TextBox();
            this.TextBoxKmViatura = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxCapacidade = new System.Windows.Forms.TextBox();
            this.TextBoxConsumoMedio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LabelQuantidadeAbastecimento = new System.Windows.Forms.Label();
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.ButtonReset = new System.Windows.Forms.Button();
            this.ButtonAbastecimentoMais = new System.Windows.Forms.Button();
            this.ButtonAbastecimentoMenos = new System.Windows.Forms.Button();
            this.ButtonEncher = new System.Windows.Forms.Button();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TextBoxMaximosKm = new System.Windows.Forms.TextBox();
            this.TextBoxReserva = new System.Windows.Forms.TextBox();
            this.TextBoxConsumoKm = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ProgressBarCombustivel = new System.Windows.Forms.ProgressBar();
            this.LabelLitrosDeposito = new System.Windows.Forms.Label();
            this.ButtonViajar = new System.Windows.Forms.Button();
            this.TextBoxKilometrosViagem = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.TextBoxViagens = new System.Windows.Forms.TextBox();
            this.TextBoxPrecoCombustivel = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.TextBoxTotalGastoEuros = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextBoxMatricula
            // 
            this.TextBoxMatricula.Location = new System.Drawing.Point(236, 59);
            this.TextBoxMatricula.Name = "TextBoxMatricula";
            this.TextBoxMatricula.Size = new System.Drawing.Size(100, 20);
            this.TextBoxMatricula.TabIndex = 0;
            // 
            // TextBoxKmViatura
            // 
            this.TextBoxKmViatura.Location = new System.Drawing.Point(236, 24);
            this.TextBoxKmViatura.Name = "TextBoxKmViatura";
            this.TextBoxKmViatura.Size = new System.Drawing.Size(100, 20);
            this.TextBoxKmViatura.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Matrícula";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Km\'s da Viatura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Capacidade Depósito";
            // 
            // TextBoxCapacidade
            // 
            this.TextBoxCapacidade.Location = new System.Drawing.Point(236, 97);
            this.TextBoxCapacidade.Name = "TextBoxCapacidade";
            this.TextBoxCapacidade.Size = new System.Drawing.Size(100, 20);
            this.TextBoxCapacidade.TabIndex = 5;
            // 
            // TextBoxConsumoMedio
            // 
            this.TextBoxConsumoMedio.Location = new System.Drawing.Point(236, 132);
            this.TextBoxConsumoMedio.Name = "TextBoxConsumoMedio";
            this.TextBoxConsumoMedio.Size = new System.Drawing.Size(100, 20);
            this.TextBoxConsumoMedio.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(205, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Consumo Médio L / 100 Km\'s";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(65, 271);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Abastecimento";
            // 
            // LabelQuantidadeAbastecimento
            // 
            this.LabelQuantidadeAbastecimento.AutoSize = true;
            this.LabelQuantidadeAbastecimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelQuantidadeAbastecimento.Location = new System.Drawing.Point(96, 298);
            this.LabelQuantidadeAbastecimento.Name = "LabelQuantidadeAbastecimento";
            this.LabelQuantidadeAbastecimento.Size = new System.Drawing.Size(20, 16);
            this.LabelQuantidadeAbastecimento.TabIndex = 11;
            this.LabelQuantidadeAbastecimento.Text = "...";
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonCriar.Location = new System.Drawing.Point(32, 176);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(75, 52);
            this.ButtonCriar.TabIndex = 12;
            this.ButtonCriar.Text = "Criar Víatura";
            this.ButtonCriar.UseVisualStyleBackColor = true;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // ButtonReset
            // 
            this.ButtonReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonReset.Location = new System.Drawing.Point(126, 176);
            this.ButtonReset.Name = "ButtonReset";
            this.ButtonReset.Size = new System.Drawing.Size(75, 52);
            this.ButtonReset.TabIndex = 13;
            this.ButtonReset.Text = "Reset Viatura";
            this.ButtonReset.UseVisualStyleBackColor = true;
            this.ButtonReset.Click += new System.EventHandler(this.ButtonReset_Click);
            // 
            // ButtonAbastecimentoMais
            // 
            this.ButtonAbastecimentoMais.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAbastecimentoMais.Location = new System.Drawing.Point(50, 317);
            this.ButtonAbastecimentoMais.Name = "ButtonAbastecimentoMais";
            this.ButtonAbastecimentoMais.Size = new System.Drawing.Size(45, 39);
            this.ButtonAbastecimentoMais.TabIndex = 14;
            this.ButtonAbastecimentoMais.Text = "+";
            this.ButtonAbastecimentoMais.UseVisualStyleBackColor = true;
            this.ButtonAbastecimentoMais.Click += new System.EventHandler(this.ButtonAbastecimentoMais_Click);
            // 
            // ButtonAbastecimentoMenos
            // 
            this.ButtonAbastecimentoMenos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAbastecimentoMenos.Location = new System.Drawing.Point(141, 317);
            this.ButtonAbastecimentoMenos.Name = "ButtonAbastecimentoMenos";
            this.ButtonAbastecimentoMenos.Size = new System.Drawing.Size(45, 39);
            this.ButtonAbastecimentoMenos.TabIndex = 15;
            this.ButtonAbastecimentoMenos.Text = "-";
            this.ButtonAbastecimentoMenos.UseVisualStyleBackColor = true;
            this.ButtonAbastecimentoMenos.Click += new System.EventHandler(this.ButtonAbastecimentoMenos_Click);
            // 
            // ButtonEncher
            // 
            this.ButtonEncher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEncher.Location = new System.Drawing.Point(265, 391);
            this.ButtonEncher.Name = "ButtonEncher";
            this.ButtonEncher.Size = new System.Drawing.Size(85, 57);
            this.ButtonEncher.TabIndex = 16;
            this.ButtonEncher.Text = "Encher Depósito";
            this.ButtonEncher.UseVisualStyleBackColor = true;
            this.ButtonEncher.Click += new System.EventHandler(this.ButtonEncher_Click);
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(92, 240);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(37, 13);
            this.LabelStatus.TabIndex = 17;
            this.LabelStatus.Text = "Status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(520, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(272, 16);
            this.label5.TabIndex = 18;
            this.label5.Text = "Com este combustivel pode percorrer:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(864, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 16);
            this.label7.TabIndex = 19;
            this.label7.Text = "Km\'s";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(586, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(206, 16);
            this.label8.TabIndex = 20;
            this.label8.Text = "Veículo já entrou na reserva:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(620, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(172, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "Consumo por Kilómetro:";
            // 
            // TextBoxMaximosKm
            // 
            this.TextBoxMaximosKm.Location = new System.Drawing.Point(798, 24);
            this.TextBoxMaximosKm.Name = "TextBoxMaximosKm";
            this.TextBoxMaximosKm.Size = new System.Drawing.Size(60, 20);
            this.TextBoxMaximosKm.TabIndex = 22;
            // 
            // TextBoxReserva
            // 
            this.TextBoxReserva.Location = new System.Drawing.Point(798, 76);
            this.TextBoxReserva.Name = "TextBoxReserva";
            this.TextBoxReserva.Size = new System.Drawing.Size(60, 20);
            this.TextBoxReserva.TabIndex = 23;
            // 
            // TextBoxConsumoKm
            // 
            this.TextBoxConsumoKm.Location = new System.Drawing.Point(798, 50);
            this.TextBoxConsumoKm.Name = "TextBoxConsumoKm";
            this.TextBoxConsumoKm.Size = new System.Drawing.Size(60, 20);
            this.TextBoxConsumoKm.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(864, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 16);
            this.label10.TabIndex = 25;
            this.label10.Text = "Litros";
            // 
            // ProgressBarCombustivel
            // 
            this.ProgressBarCombustivel.Location = new System.Drawing.Point(568, 105);
            this.ProgressBarCombustivel.Name = "ProgressBarCombustivel";
            this.ProgressBarCombustivel.Size = new System.Drawing.Size(235, 23);
            this.ProgressBarCombustivel.TabIndex = 26;
            this.ProgressBarCombustivel.Click += new System.EventHandler(this.ProgressBarCombustivel_Click);
            // 
            // LabelLitrosDeposito
            // 
            this.LabelLitrosDeposito.AutoSize = true;
            this.LabelLitrosDeposito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLitrosDeposito.Location = new System.Drawing.Point(830, 108);
            this.LabelLitrosDeposito.Name = "LabelLitrosDeposito";
            this.LabelLitrosDeposito.Size = new System.Drawing.Size(58, 16);
            this.LabelLitrosDeposito.TabIndex = 27;
            this.LabelLitrosDeposito.Text = "0 Litros";
            // 
            // ButtonViajar
            // 
            this.ButtonViajar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonViajar.Location = new System.Drawing.Point(786, 378);
            this.ButtonViajar.Name = "ButtonViajar";
            this.ButtonViajar.Size = new System.Drawing.Size(75, 71);
            this.ButtonViajar.TabIndex = 28;
            this.ButtonViajar.Text = "Viajar!";
            this.ButtonViajar.UseVisualStyleBackColor = true;
            this.ButtonViajar.Click += new System.EventHandler(this.ButtonViajar_Click);
            // 
            // TextBoxKilometrosViagem
            // 
            this.TextBoxKilometrosViagem.Location = new System.Drawing.Point(661, 429);
            this.TextBoxKilometrosViagem.Name = "TextBoxKilometrosViagem";
            this.TextBoxKilometrosViagem.Size = new System.Drawing.Size(100, 20);
            this.TextBoxKilometrosViagem.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(643, 390);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(137, 16);
            this.label11.TabIndex = 30;
            this.label11.Text = "Kilómetros a viajar";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(643, 135);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(149, 16);
            this.label12.TabIndex = 31;
            this.label12.Text = "Número de Viagens:";
            // 
            // TextBoxViagens
            // 
            this.TextBoxViagens.Location = new System.Drawing.Point(798, 132);
            this.TextBoxViagens.Name = "TextBoxViagens";
            this.TextBoxViagens.Size = new System.Drawing.Size(60, 20);
            this.TextBoxViagens.TabIndex = 32;
            // 
            // TextBoxPrecoCombustivel
            // 
            this.TextBoxPrecoCombustivel.Location = new System.Drawing.Point(76, 418);
            this.TextBoxPrecoCombustivel.Name = "TextBoxPrecoCombustivel";
            this.TextBoxPrecoCombustivel.Size = new System.Drawing.Size(100, 20);
            this.TextBoxPrecoCombustivel.TabIndex = 33;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(29, 390);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(216, 16);
            this.label13.TabIndex = 34;
            this.label13.Text = "Preço do Litro de Combustivel";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(554, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(238, 16);
            this.label14.TabIndex = 35;
            this.label14.Text = "Valor total gasto em combustivel:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(864, 164);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 16);
            this.label15.TabIndex = 36;
            this.label15.Text = "Euros";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(186, 419);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 16);
            this.label16.TabIndex = 37;
            this.label16.Text = "Euros";
            // 
            // TextBoxTotalGastoEuros
            // 
            this.TextBoxTotalGastoEuros.Location = new System.Drawing.Point(798, 163);
            this.TextBoxTotalGastoEuros.Name = "TextBoxTotalGastoEuros";
            this.TextBoxTotalGastoEuros.Size = new System.Drawing.Size(60, 20);
            this.TextBoxTotalGastoEuros.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(122, 298);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 16);
            this.label17.TabIndex = 39;
            this.label17.Text = "Litros";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 474);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.TextBoxTotalGastoEuros);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.TextBoxPrecoCombustivel);
            this.Controls.Add(this.TextBoxViagens);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TextBoxKilometrosViagem);
            this.Controls.Add(this.ButtonViajar);
            this.Controls.Add(this.LabelLitrosDeposito);
            this.Controls.Add(this.ProgressBarCombustivel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TextBoxConsumoKm);
            this.Controls.Add(this.TextBoxReserva);
            this.Controls.Add(this.TextBoxMaximosKm);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.ButtonEncher);
            this.Controls.Add(this.ButtonAbastecimentoMenos);
            this.Controls.Add(this.ButtonAbastecimentoMais);
            this.Controls.Add(this.ButtonReset);
            this.Controls.Add(this.ButtonCriar);
            this.Controls.Add(this.LabelQuantidadeAbastecimento);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextBoxConsumoMedio);
            this.Controls.Add(this.TextBoxCapacidade);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxKmViatura);
            this.Controls.Add(this.TextBoxMatricula);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxMatricula;
        private System.Windows.Forms.TextBox TextBoxKmViatura;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxCapacidade;
        private System.Windows.Forms.TextBox TextBoxConsumoMedio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LabelQuantidadeAbastecimento;
        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Button ButtonReset;
        private System.Windows.Forms.Button ButtonAbastecimentoMais;
        private System.Windows.Forms.Button ButtonAbastecimentoMenos;
        private System.Windows.Forms.Button ButtonEncher;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TextBoxMaximosKm;
        private System.Windows.Forms.TextBox TextBoxReserva;
        private System.Windows.Forms.TextBox TextBoxConsumoKm;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ProgressBar ProgressBarCombustivel;
        private System.Windows.Forms.Label LabelLitrosDeposito;
        private System.Windows.Forms.Button ButtonViajar;
        private System.Windows.Forms.TextBox TextBoxKilometrosViagem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TextBoxViagens;
        private System.Windows.Forms.TextBox TextBoxPrecoCombustivel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TextBoxTotalGastoEuros;
        private System.Windows.Forms.Label label17;
    }
}

