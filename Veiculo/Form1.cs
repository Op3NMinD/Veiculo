﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Veiculo
{
    public partial class Form1 : Form
    {
        //Regex matricula = new Regex("^[0-9]{2}-[a-zA-Z]{2}-[0-9]{2}$");
        double contem, media, km;
        private Veiculo veiculo;
      

        public Form1()
        {
            InitializeComponent();

            ButtonReset.Enabled = false;
            ButtonAbastecimentoMais.Enabled = false;
            ButtonAbastecimentoMenos.Enabled = false;
            ButtonEncher.Enabled = false;
            TextBoxReserva.Enabled = false;
            TextBoxConsumoKm.Enabled = false;
            TextBoxMaximosKm.Enabled = false;
            TextBoxViagens.Enabled = false;
            TextBoxTotalGastoEuros.Enabled = false;
            TextBoxPrecoCombustivel.Enabled = false;
            ButtonViajar.Enabled = false;
            TextBoxKilometrosViagem.Enabled = false;
            TextBoxKmViatura.Enabled = false;

        }


        private void ButtonCriar_Click(object sender, EventArgs e)
        {

            //Match match = matricula.Match(TextBoxMatricula.Text);

            if (TextBoxMatricula.Text == "")
            {
                LabelStatus.Text = "Introduza uma matricula!";
                return;
            }

            if (TextBoxCapacidade.Text == "")
            {
                LabelStatus.Text = "Introduza a Capacidade do depósito da viatura!";
                return;
            }

            if (TextBoxConsumoMedio.Text == "")
            {
                LabelStatus.Text = "Introduza o consumo médio da viatura!";
                return;
            }
            else
            {
                veiculo = new Veiculo(TextBoxMatricula.Text, Convert.ToDouble(TextBoxCapacidade.Text), Convert.ToDouble(TextBoxConsumoMedio.Text));
                LabelStatus.Text = "Viatura Criada com sucesso!";
                ButtonReset.Enabled = true;
                ButtonAbastecimentoMais.Enabled = true;
                ButtonAbastecimentoMenos.Enabled = true;
                ButtonEncher.Enabled = true;
                ButtonCriar.Enabled = false;
                TextBoxMatricula.Enabled = false;
                TextBoxCapacidade.Enabled = false;
                TextBoxKmViatura.Enabled = false;
                TextBoxConsumoMedio.Enabled = false;               
                media = Convert.ToDouble(TextBoxConsumoMedio.Text);
                contem = veiculo.Contem;                
                TextBoxReserva.Text = veiculo.ReservaCombustivel(contem);
                TextBoxConsumoKm.Text = veiculo.ConsumoPorLitro(media).ToString();
                TextBoxMaximosKm.Text = veiculo.KilometrosPercorridos(media, contem).ToString();
                TextBoxViagens.Text = veiculo.Viagens.ToString();
                TextBoxPrecoCombustivel.Enabled = true;
                ButtonViajar.Enabled = true;
                TextBoxKilometrosViagem.Enabled = true;
                TextBoxKmViatura.Text = veiculo.Kilometragem.ToString();
                TextBoxTotalGastoEuros.Text = "0";       
            }

        }

        private void ButtonAbastecimentoMais_Click(object sender, EventArgs e)
        {
            if (contem < veiculo.Capacidade)
            {
                contem++;
                LabelQuantidadeAbastecimento.Text = contem.ToString();
                
            }     
        }

        private void ProgressBarCombustivel_Click(object sender, EventArgs e)
        {

        }

        private void ButtonAbastecimentoMenos_Click(object sender, EventArgs e)
        {
            if (contem > 0)
            {
                contem--;
                LabelQuantidadeAbastecimento.Text = contem.ToString();
            }

        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void ButtonEncher_Click(object sender, EventArgs e)
        {
            veiculo.Encher(contem);
            TextBoxReserva.Text = veiculo.ReservaCombustivel(veiculo.Contem);
            TextBoxMaximosKm.Text = veiculo.KilometrosPercorridos(media, veiculo.Contem).ToString();
            ProgressBarCombustivel.Value = veiculo.ValorEmPercentagem();
            LabelLitrosDeposito.Text = veiculo.Contem.ToString() + " Litros";         
            TextBoxTotalGastoEuros.Text = veiculo.TotalGastoAteHoje(Convert.ToDouble(TextBoxPrecoCombustivel.Text), Convert.ToDouble(TextBoxTotalGastoEuros.Text), Convert.ToDouble(LabelQuantidadeAbastecimento.Text)).ToString();
        }
        private void ButtonViajar_Click(object sender, EventArgs e)
        {
            km = Convert.ToDouble(TextBoxKilometrosViagem.Text);
            if (km <= veiculo.KilometrosPercorridos(media, veiculo.Contem))
            {
                veiculo.Viajar(km);
                TextBoxReserva.Text = veiculo.ReservaCombustivel(veiculo.Contem);
                TextBoxMaximosKm.Text = veiculo.KilometrosPercorridos(media, veiculo.Contem).ToString();
                ProgressBarCombustivel.Value = veiculo.ValorEmPercentagem();
                TextBoxKmViatura.Text = veiculo.Kilometragem.ToString();
                LabelLitrosDeposito.Text = veiculo.Contem.ToString() + " Litros";
                TextBoxViagens.Text = veiculo.Viagens.ToString();

            }
            else
                MessageBox.Show("Não tens gasolina para esses Kilómetros! :D");
                return;
        }

    }
}
